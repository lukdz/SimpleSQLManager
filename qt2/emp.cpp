#include <QCoreApplication>
#include <QtSql>
#include <iostream>

using namespace std;

#include "emp.hpp"

void show_emp(){
    qInfo() << "show_emp start";
    QSqlQuery query(db);
    query.prepare("SELECT pesel, first_name, last_name FROM employees");
    query.exec();
    qInfo() << "PESEL" << "\t\t" << "imię" << "\t" << "nazwisko";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}


void show_cops(){
    qInfo() << "show_cops start";
    QSqlQuery query(db);
    query.prepare("SELECT pesel, rank, first_name, last_name FROM cops JOIN employees USING(pesel)");
    query.exec();
    qInfo() << "PESEL" << "\t" << "stopień" << "\t" << "imię" << "\t" << "nazwisko";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        QString w3 = query.value(3).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2 << "\t" << w3;
    }
}


void show_civ(){
    qInfo() << "show_civ start";
    QSqlQuery query(db);
    query.prepare("SELECT pesel, position, first_name, last_name FROM civilians JOIN employees USING(pesel)");
    query.exec();
    qInfo() << "PESEL" << "\t" << "stanowisko" << "\t" << "imię" << "\t" << "nazwisko";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        QString w3 = query.value(3).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2 << "\t" << w3;
    }
}


void show_cars(){
    qInfo() << "show_cars start";
    QSqlQuery query(db);
    query.prepare("SELECT vin, reg_number, last_name FROM cars JOIN employees ON(owner=pesel)");
    query.exec();
    qInfo() << "Nr vin" << "\t" << "Nr rejestracyjny" << "\t" << "nazwisko";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}

void show_guns(){
    qInfo() << "show_guns start";
    QSqlQuery query(db);
    query.prepare("SELECT serial_number, model, last_name FROM guns JOIN employees ON(owner=pesel)");
    query.exec();
    qInfo() << "Nr seryjny" << "\t" << "model" << "\t" << "nazwisko";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}

void show_eq(){
    qInfo() << "show_eq start";
    QSqlQuery query(db);
    query.prepare("SELECT name, quantity, value FROM equipment");
    query.exec();
    qInfo() << "Nazwa" << "\t" << "ilość" << "\t" << "wartość";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}

void show_dogs(){
    qInfo() << "show_dogs start";
    QSqlQuery query(db);
    query.prepare("SELECT name, date_part('year', now())-year_of_birth, last_name FROM police_dogs JOIN employees ON(owner=pesel)");
    query.exec();
    qInfo() << "Imię" << "\t" << "wiek" << "\t" << "Nazwisko właściciela";
    while(query.next()){
        QString w0 = query.value(0).toString();
        QString w1 = query.value(1).toString();
        QString w2 = query.value(2).toString();
        qInfo() << w0 << "\t" << w1 << "\t" << w2;
    }
}


