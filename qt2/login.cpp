#include <QCoreApplication>
#include <QtSql>
#include <iostream>

using namespace std;

#include "login.hpp"
#include "menu.hpp"
#include "admin.hpp"


void login(){
    qInfo() << "Proszę się zalogować";
    string log;
    string pas;
    while(true){
        cout << "Login: ";
        cin >> log;
        cout << "Hasło: ";
        cin >> pas;



        QSqlQuery query(db);
        query.prepare("SELECT login, password, permission_level FROM employees");
        query.exec();
        while(query.next()){
            QString w0 = query.value(0).toString();
            QString w1 = query.value(1).toString();
            if(w0.toUtf8().constData() == log && w1.toUtf8().constData() == pas){
                if(query.value(2).toInt() == 0){
                    admin();
                    qInfo() << "Admin end";
                    return;
                }
                else{
                    menu();
                    return;
                }
            }
        }
        qInfo() << "Niepoprawne dane, spróbuj jeszcze raz";
    }

}
