INSERT INTO employees(pesel, login, password, permission_level) VALUES
('123456789', 'admin', '123', 0);
INSERT INTO employees(pesel, login, password, first_name, last_name) VALUES
('007', 'bond', 'bond', 'James', 'Bond');
INSERT INTO employees(pesel, login, password, first_name, last_name) VALUES
('1', 'wasd', 'luty201', 'Kuba', 'Sawicki');
INSERT INTO employees(pesel, login, password, first_name, last_name) VALUES
('2', 'marek', 'skip', 'Marek', 'Dzwoniarek');
INSERT INTO employees(pesel, login, password, first_name, last_name) VALUES
('3', 'krysia', 'ala12', 'Krysia', 'Kubicka');


INSERT INTO cops(pesel, rank, wage) VALUES
('007', 'agent', '1000');
INSERT INTO cops(pesel, rank, wage) VALUES
('1', 'sierżant', '900');


INSERT INTO civilians(pesel, position, wage) VALUES
('3', 'k', '999');
INSERT INTO civilians(pesel, position, wage) VALUES
('2', 'm', '950');


INSERT INTO citizens(pesel, first_name, last_name) VALUES
('151', 'Zbyszek', 'Baniewski');
INSERT INTO citizens(pesel, first_name, last_name) VALUES
('142', 'Władek', 'Krętalski');
INSERT INTO citizens(pesel, first_name, last_name) VALUES
('111', 'Aleks', 'Krawczyk');

INSERT INTO cars(vin, reg_number, owner) VALUES
('112', 'DW 5464', '1');
INSERT INTO cars(vin, reg_number, owner) VALUES
('113', 'DW 404', '007');


INSERT INTO guns(serial_number, model, owner) VALUES
('2873345', 'Magnum', '007');
INSERT INTO guns(serial_number, model, owner) VALUES
('2322345', 'Glock', '1');

INSERT INTO equipment(name, quantity, value) VALUES
('Mop', '2', '10');
INSERT INTO equipment(name, quantity, value) VALUES
('Pały', '666', '100');


INSERT INTO police_dogs(name, owner, year_of_birth) VALUES
('Birek', '1', '2010');
INSERT INTO police_dogs(name, owner, year_of_birth) VALUES
('Rudy', '007', '2008');

INSERT INTO investigations(case_number, officer, description) VALUES
('WK2/8', '1', 'Kradzież kostki brukowej');
INSERT INTO investigations(case_number, officer, description) VALUES
('WP2/344', '007', 'Włamanie z pobiciem');
